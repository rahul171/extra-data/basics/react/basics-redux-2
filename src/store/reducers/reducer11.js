import { createReducer } from 'redux-act';
import {
  increaseR11,
  decreaseR11,
  randomEventR11,
  messageR11,
} from '../actions/action2';

const initialState = {
  value: 11,
  message: 'no message',
};

export const reducer11 = createReducer(
  {
    [increaseR11]: (state, payload) => state.value + 1,
    [decreaseR11]: (state, payload) => state.value - 1,
    [messageR11]: (state, { message }) => {
      state.message = message;
      return state;
    },
    [randomEventR11]: (state, payload) => {
      console.log('in here');
      const newState = { ...state };
      newState.value = payload.value;
      newState.message = payload.message;
      return newState;
    },
  },
  initialState,
);
