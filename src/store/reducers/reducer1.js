const initialState = {
  value: 1,
};

export const reducer1 = (state = initialState, action) => {
  const newState = { ...state };

  if (action.type === 'increaseR1') {
    newState.value++;
  } else if (action.type === 'decreaseR1') {
    newState.value--;
  } else if (action.type === 'randomEventR1') {
    newState.value = action.payload.value;
    newState.message = action.payload.message;
  } else if (action.type === 'randomEventWaitR1') {
    newState.message = action.payload.message;
  }

  return newState;
};
