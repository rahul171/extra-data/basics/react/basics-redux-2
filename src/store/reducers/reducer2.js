const initialState = {
  value: 11,
};

export const reducer2 = (state = initialState, action) => {
  const newState = { ...state };

  if (action.type === 'increaseR2') {
    newState.value++;
  } else if (action.type === 'decreaseR2') {
    newState.value--;
  } else if (action.type === 'randomEventR2') {
    newState.value = action.payload.value;
  }

  return newState;
};
