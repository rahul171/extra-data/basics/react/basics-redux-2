import { createAction } from 'redux-act';

export const increaseR11 = createAction('increase r11');

export const decreaseR11 = createAction('decrease r11');

export const randomEventR11 = createAction(
  'random event r11',
  (value, message) => ({
    value,
    message,
  }),
);

export const messageR11 = createAction('message R11', (message) => ({
  message,
}));

export const randomEventR11Async = createAction(
  'random event r11 async',
  (value) => async (dispatch, getState) => {
    dispatch(messageR11('waiting for the data for R11...'));

    const newValue = await new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(value + 10);
      }, 2000);
    });

    dispatch(randomEventR11(newValue, ''));
  },
);
