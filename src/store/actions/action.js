export const increase = () => ({ type: 'increaseR1' });
export const decrease = () => ({ type: 'decreaseR1' });
export const randomEvent = (value) => async (dispatch, getState) => {
  dispatch({
    type: 'randomEventWaitR1',
    payload: { message: 'waiting for the value...' },
  });

  const newValue = await new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(value + 10);
    }, 2000);
  });

  dispatch({
    type: 'randomEventR1',
    payload: { value: newValue, message: '' },
  });
};
