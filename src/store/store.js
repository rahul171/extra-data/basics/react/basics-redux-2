import { reducer1, reducer11, reducer2 } from './reducers';
import { applyMiddleware, combineReducers, createStore } from 'redux';
import { middleware1, middleware2 } from './middlewares';
import { composeWithDevTools } from 'redux-devtools-extension';

const reducer = combineReducers({
  r1: reducer1,
  r2: reducer2,
  r11: reducer11,
});

const composeEnhancers = composeWithDevTools({ trace: true, traceLimit: 25 });

const store = createStore(
  reducer,
  composeEnhancers(applyMiddleware(middleware1, middleware2)),
);

export default store;
