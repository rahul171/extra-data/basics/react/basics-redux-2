export const middleware1 = (store) => (next) => (action) => {
  console.log('middleware 1', typeof action, action);
  if (typeof action === 'function') {
    action(store.dispatch, store.getState);
  } else {
    next(action);
  }
};
