import { createReducer } from 'redux-act';
import {
  increaseR11,
  decreaseR11,
  randomEventR11,
  messageR11,
} from '../actions/action11';
import { produce } from 'immer';

const initialState = {
  value: 11,
  message: 'no message',
};

export const reducer11 = createReducer(
  {
    [increaseR11]: produce((draft, payload) => {
      draft.value++;
    }),
    [decreaseR11]: produce((draft, payload) => {
      draft.value--;
    }),
    [messageR11]: produce((draft, { message }) => {
      draft.message = message;
    }),
    [randomEventR11]: produce((draft, payload) => {
      console.log('in here');
      draft.value = payload.value;
      draft.message = payload.message;
    }),
  },
  initialState,
);
