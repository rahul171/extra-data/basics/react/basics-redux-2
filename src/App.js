import React from 'react';
import { connect } from 'react-redux';
import {
  decreaseR11,
  increaseR11,
  randomEventR11,
  randomEventR11Async,
} from './store2/actions/action11';
import * as action11 from './store2/actions/action11';
import { bindActionCreators } from 'redux';

function App(props) {
  return (
    <div>
      <div>{props.valueR11}</div>
      <button onClick={props.increaseR11}>increaseR1</button>
      <button onClick={props.decreaseR11}>decreaseR1</button>
      <button
        onClick={() => {
          props.randomEventR11(Math.floor(101 * Math.random()));
        }}
      >
        randomEventR11
      </button>
      <button
        onClick={() => {
          props.randomEventR11Async(Math.floor(101 * Math.random()));
        }}
      >
        randomEventR11Async
      </button>
      <div>{props.messageR11}</div>
    </div>
  );
}

const mapStateToProps = (store) => ({
  valueR11: store.r11.value,
  messageR11: store.r11.message,
});

const mapDispatchToProps = (dispatch) => ({
  increaseR11: () => dispatch(increaseR11()),
  decreaseR11: () => dispatch(decreaseR11()),
  randomEventR11: (value) => dispatch(randomEventR11(value)),
  randomEventR11Async: (value) => dispatch(randomEventR11Async(value)),
});

// const mapDispatchToProps = (dispatch) => ({
//   actions: bindActionCreators(action11, dispatch),
// });

export default connect(mapStateToProps, mapDispatchToProps)(App);
